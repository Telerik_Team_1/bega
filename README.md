# Movie Quotes API

## Contributors:
Team 1

Team leader: Stoyan

Team members: [Stoyan](https://my.telerikacademy.com/Users/ogonommo), [Nikola](https://my.telerikacademy.com/Users/nizlatinov)

## Project Description:

- Application providing movie quotes to registered users via **REST** api.
- The information needed by the app is stored in a **MariaDB** database.
- The aplication is created using **Nest**.
- **TypeORM** is used to manipulate the database.

## Project Features:

 ### Authentication

 The authentication controller provides functionality allowing users to:
  - **register** to use the api by providing username and password
  - **login** and access the API constent according to their role
  - authentication is based on JWT.

 ### Authorization

  - Users can have the following roles:
    - **admin**
    - **user**
    - **banned**

  Default role is "user" and can be changed by admin.
  It is kept in the database and is a part of the JWT body.

 ### Users

 The users controller provides functionality allowing users/admins to access or manipulate users data according to their role.
  - normal users can:
    - access individual users by 'id'
    - change their own password.
  - admins can:
    - access user by 'id'
    - view users list
    - change roles of users
    - change their own password.

 ### Movies

 The movies controller provides functionality allowing users/admins to access or manipulate movies data according to their role.
  - normal users can:
    - access list of all movies
    - individual movie by 'id'
  - admins can:
    - add a movie
    - view movies list
    - view a movie by its 'id'
    - update movie by 'id' if they are the one that added it
    - delete a movie by its 'id'

 ### Quotes

 The quotes controller provides functionality allowing users/admins to access or manipulate quotes data according to their role.
 - normal users can:
   - get all quotes
   - get quote by 'id'
   - add quote to a movie with certain 'id'
   - edit a quote by 'id' if they are the one that added it
 - admins have the same rights to manipulate quotes as normal uses, with the exeption that they can also delete a quote by its 'id'

## Trello:

[Board](https://trello.com/b/SBQq4vGS/movie-quotes)
