import { Request, Controller, Get, UseGuards, Put, Body, ValidationPipe, Param, ParseIntPipe, Delete, Patch } from '@nestjs/common';
import { QuotesService } from './quotes.service';
import { AuthGuard } from '@nestjs/passport';
import { QuoteAddSchema } from '../models/quotes/quote-add.schema';
import { RolesGuard, Roles, EditQhote } from '../common';
import { QuoteSchema } from '../models/quotes/quote.schema';
import { Quote } from '../data/entities/quote.entity';

@Controller('quotes')
@UseGuards(AuthGuard(), RolesGuard)
export class QuotesController {
  constructor(
    public readonly quotesService: QuotesService,
  ) {}

  @Get()
  @Roles('admin', 'user')
    async getAll(): Promise<QuoteSchema[]> {

    return this.quotesService.getAll();
  }

  @Get(':quoteId')
  @Roles('admin', 'user')
    getById(@Param('quoteId', new ParseIntPipe()) id: number): Promise<QuoteSchema> {

    return this.quotesService.getById(id);
  }

  @Put(':movieId')
  @Roles('admin', 'user')
  async addQuote(
    @Param('movieId', new ParseIntPipe()) movieId: number,
    @Request() req: any,
    @Body(new ValidationPipe({
      transform: true,
      whitelist: true,
    })) quote: QuoteAddSchema): Promise<Quote> {

      try{
       return await this.quotesService.add(movieId, quote, req.user);
      } catch (err) {
        return err.message;
      }
  }

  @Delete('/:id')
  @Roles('admin')
  async deleteById(@Param('id', new ParseIntPipe()) id: number): Promise<Quote> {

    return await this.quotesService.deleteById(id);
  }

  @Patch('/:id')
  @UseGuards(EditQhote)
  async updateById(
    @Param('id', new ParseIntPipe()) quoteId: number,
    @Body(new ValidationPipe({
      transform: true,
      whitelist: true,
    })) quote: QuoteAddSchema,
    ): Promise<Quote> {

    return await this.quotesService.updateById(quoteId, quote);
  }
}
