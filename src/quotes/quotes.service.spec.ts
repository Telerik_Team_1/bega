import { Test, TestingModule } from '@nestjs/testing';
import { QuotesService } from './quotes.service';
import { Quote } from '../data/entities/quote.entity';
import { QuoteSchema } from '../models/quotes/quote.schema';
import { QuoteAddSchema } from '../models/quotes/quote-add.schema';
import { User } from '../data/entities/user.entity';

describe('QuotesService', () => {
  let service: QuotesService;
  const quoteRepository = {
    find: () => {
      return 'quote';
    },
    findOne: () => {
      return 'quote';
    },
    create: () => {
      return 'quote';
    },
    save: () => {
      return 'quote';
    },
    remove: () => {
      return 'quote';
    },
  };

  beforeAll(async () => {

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuotesService,
        {
          provide: 'QuoteRepository',
          useValue: quoteRepository,
        },
      ],
    }).compile();

    service = module.get<QuotesService>(QuotesService);
  });

  afterEach(async () => {
    jest.clearAllMocks();
  });

  it(' should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('.getAll ', () => {

    it(' should call QuoteRepository.find ', async () => {

      // Arrange
      jest.spyOn(quoteRepository, 'find');

      // Act
      await service.getAll();

      // Assert
      expect(quoteRepository.find).toHaveBeenCalledTimes(1);
    });

    it(' should return the correct data from quoteService', async () => {

      // Arrange
      const quote = new Quote();
      jest.spyOn(quoteRepository, 'find').mockReturnValue([quote]);

      // Act
      const result = await service.getAll();

      // Assert
      expect(result[0]).toBeInstanceOf(QuoteSchema);
    });
  });

  describe('getById(parameter)', () => {

    it('should call quoteRepository.findOne', () => {

      // Arrange
      jest.spyOn(quoteRepository, 'findOne');

      // Act
      service.getById(1);

      // Assert
      expect(quoteRepository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should return correct data from quoteService', async () => {

      // Arrange
      const quote = new Quote();
      jest.spyOn(quoteRepository, 'findOne').mockReturnValue(quote);

      // Act
      const result = await service.getById(1);

      // Assert
      expect(result).toBeInstanceOf(QuoteSchema);
    });

    it('should trow if no movie is found', async () => {

      // Arrange
      let result;
      jest.spyOn(quoteRepository, 'findOne').mockReturnValue(undefined);

      // Act
      try{
        await service.getById(1);
      } catch (error) {
        result = error.message.message;
      }

      // Assert
      expect(result).toBe(`The quote you are trying to access does not exist!`);
    });
  });

  describe('.add ', () => {

    const quoteData: QuoteAddSchema = {quotetext: 'text'};
    const user: User = new User();

    it(' should trow when quote already exist.', async () => {

      // Arrange
      jest.spyOn(quoteRepository, 'findOne').mockReturnValue(true);
      let result = null;

      // Act
      try{
        await service.add(1, quoteData, user);
      } catch (error) {
        result = error.message.message;
      }

      // Assert
      expect(result).toBe('This quote already exists!');
    });
  });

  describe('.deleteById ', () => {

    it(' should call QuotesRepository.findOne with parameter provided ', async () => {

      // Arrange
      jest.spyOn(quoteRepository, 'findOne');

      // Act
      await service.deleteById(2);

      // Assert
      expect(quoteRepository.findOne).toHaveBeenCalledTimes(1);
      expect(quoteRepository.findOne).toHaveBeenCalledWith({id: 2});
    });

    it(' should call quotesRepository.remove with parameter provided', async () => {

      // Arrange
      const quote = new Quote();
      jest.spyOn(quoteRepository, 'findOne').mockReturnValue(quote);
      jest.spyOn(quoteRepository, 'remove');

      // Act
      await service.deleteById(1);

      // Assert
      expect(quoteRepository.remove).toBeCalledTimes(1);
      expect(quoteRepository.remove).toBeCalledWith(quote);
    });

    it(' should throw if quote does not exist.', async () => {

      // Arrange
      let result = null;
      jest.spyOn(quoteRepository, 'findOne').mockReturnValue(undefined);
      jest.spyOn(quoteRepository, 'remove');

      // Act
      try {
        await service.deleteById(1);
      } catch (error) {
        result = error.message.message;
      }

      // Assert
      expect(result).toBe(`The quote you are trying to delete does not exist!`);
    });
  });

  describe('.updateById ', () => {

    const quoteData: QuoteAddSchema = {quotetext: 'text'};

    it(' should trow when quote exists ', async () => {

      // Arrange
      jest.spyOn(quoteRepository, 'findOne').mockReturnValue(true);
      let result = null;

      // Act
      try {
        await service.updateById(2, quoteData);
      } catch (error) {
        result = error.message.message;
      }

      // Assert
      expect(result).toBe('This quote already exists!');
    });

    it(' should trow when quote with such id does not exist ', async () => {

      // Arrange
      jest.spyOn(quoteRepository, 'findOne').mockReturnValue(false);
      let result = null;

      // Act
      try {
        await service.updateById(2, quoteData);
      } catch (error) {
        result = error.message.message;
      }

      // Assert
      expect(result).toBe('You are trying to modify a quote with non-existing id!');
    });
  });
});