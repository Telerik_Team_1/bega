import { Test, TestingModule } from '@nestjs/testing';
import { QuotesController } from './quotes.controller';
import { QuotesService } from './quotes.service';
import { QuoteAddSchema } from '../models/quotes/quote-add.schema';
import { BadRequestException } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

jest.mock('./quotes.service');

describe('Quotes Controller', () => {

  let module: TestingModule;
  const quote: QuoteAddSchema = new QuoteAddSchema();
  let controller: QuotesController;
  const quotesService: QuotesService = new QuotesService(null);

  beforeAll(async () => {

    module = await Test.createTestingModule({
      imports: [PassportModule.register({ defaultStrategy: 'jwt' })],
      controllers: [QuotesController],
      providers: [
        {
          provide: 'QuotesService',
          useValue: quotesService,
        },
      ],
    }).compile();
    controller = module.get<QuotesController>(QuotesController);
  });
  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('.getAll', () => {

    it(' should call quotesService.getAll.', async () => {

      // Arrange
      jest.spyOn(quotesService, 'getAll');

      // Act
      await controller.getAll();

      // Assert
      expect(quotesService.getAll).toHaveBeenCalledTimes(1);
    });

    it(' should return what it received from quotesService.getAll.', async () => {

      // Arrange
      jest.spyOn(quotesService, 'getAll').mockReturnValue(quote);

      // Act
      const result = await controller.getAll();

      // Assert
      expect(result).toBe(quote);
    });
  });

  describe('.getById', () => {
    it(' should call quotesService.getById.', async () => {

      // Arrange
      jest.spyOn(quotesService, 'getById');

      // Act
      await controller.getById(1);

      // Assert
      expect(quotesService.getById).toHaveBeenCalledTimes(1);
    });

    it(' should return what it received from quotesService.getById.', async () => {

      // Arrange
      jest.spyOn(quotesService, 'getById').mockReturnValue(quote);

      // Act
      const result = await controller.getById(1);

      // Assert
      expect(result).toBe(quote);
    });
  });

  describe('.addQuote', () => {
    it(' should call quotesService.add.', async () => {

      // Arrange
      jest.spyOn(quotesService, 'add').mockReturnValue(true);

      // Act
      await controller.addQuote(1, {user: 'User'}, quote);

      // Assert
      expect(quotesService.add).toHaveBeenCalledTimes(1);
    });

    it(' should return what it received from quotesService.add.', async () => {

      // Arrange
      jest.spyOn(quotesService, 'add').mockReturnValue(quote);

      // Act
      const result = await controller.addQuote(1, {user: 'User'}, quote);

      // Assert
      expect(result).toBe(quote);
    });
  });

  describe('.deleteById', () => {
    it(' should call quotesService.deleteById.', async () => {

      // Arrange
      jest.spyOn(quotesService, 'deleteById');

      // Act
      await controller.deleteById(1);

      // Assert
      expect(quotesService.deleteById).toHaveBeenCalledTimes(1);
    });

    it(' should return what it received from quotesService.deleteById.', async () => {

      // Arrange
      jest.spyOn(quotesService, 'deleteById').mockReturnValue(quote);

      // Act
      const result = await controller.deleteById(1);

      // Assert
      expect(result).toBe(quote);
    });
  });

  describe('.updateById', () => {
    it(' should call quotesService.updateById.', async () => {

      // Arrange
      jest.spyOn(quotesService, 'updateById');

      // Act
      await controller.updateById(1, quote);

      // Assert
      expect(quotesService.updateById).toHaveBeenCalledTimes(1);
    });

    it(' should return what it received from quotesService.updateById.', async () => {

      // Arrange
      jest.spyOn(quotesService, 'updateById').mockReturnValue(quote);

      // Act
      const result = await controller.updateById(1, quote);

      // Assert
      expect(result).toBe(quote);
    });
  });
});