import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Quote } from '../data/entities/quote.entity';
import { Repository, getRepository } from 'typeorm';
import { QuoteSchema } from '../models/quotes/quote.schema';
import { QuoteAddSchema } from '../models/quotes/quote-add.schema';
import { User } from '../data/entities/user.entity';
import { Movie } from '../data/entities/movie.entity';

@Injectable()
export class QuotesService {

  constructor(
    @InjectRepository(Quote)
    private readonly quoteRepository: Repository<Quote>,
  ) {}

  async add(movieId: number, quote: QuoteAddSchema, user: User): Promise<Quote>{

      const quoteExists = await this.quoteRepository.findOne({ where: { quotetext: quote.quotetext } });

      if (quoteExists) {
        throw new BadRequestException('This quote already exists!');
      }

      const newQuote = new Quote();

      newQuote.quotetext = quote.quotetext;
      newQuote.status = 1; // magic 1. stands for mistery.
      newQuote.user = Promise.resolve(user);
      newQuote.movie = Promise.resolve(await getRepository(Movie).findOne({id: movieId})); // temporary fix

      await this.quoteRepository.create(newQuote);

      const result = await this.quoteRepository.save(newQuote);

      return await this.quoteRepository.findOne({ where: { quotetext: quote.quotetext } });
    }

  async getAll(): Promise<QuoteSchema[]> {

    const quotes = await this.quoteRepository.find({});
    const result: any[] = [];

    for (const quote of quotes) {
      result.push(await this.mapQuote(quote));
    }

    return result;
  }

  // cannot await it when used inline as callback in array.map() operator
  async mapQuote(quote: Quote): Promise<QuoteSchema> {
    const newQuoteSchema: QuoteSchema = new QuoteSchema();

    newQuoteSchema.id = quote.id;
    newQuoteSchema.movie = await quote.movie;
    newQuoteSchema.user = await quote.user;
    newQuoteSchema.quotetext = quote.quotetext;
    newQuoteSchema.status = quote.status;

    return newQuoteSchema;
  }

  async getById(ID: number): Promise<QuoteSchema> {

    const quoteFound: Quote = await this.quoteRepository.findOne({ where: { id: ID} });

    if (quoteFound) {
      return await this.mapQuote(quoteFound);
    } else {
      throw new BadRequestException(`The quote you are trying to access does not exist!`);
    }
  }

  async deleteById(ID: number): Promise<Quote> {

    const quoteToDelete: Quote = await this.quoteRepository.findOne({id: ID});

    if (quoteToDelete) {
      await this.quoteRepository.remove(quoteToDelete);
      return quoteToDelete;
    } else {
      throw new BadRequestException(`The quote you are trying to delete does not exist!`);
    }
  }

  async updateById(quoteId: number, quoteData: QuoteAddSchema): Promise<Quote> {

    const quoteExists = await this.quoteRepository.findOne({quotetext: quoteData.quotetext});

    if (quoteExists) {
      throw new BadRequestException('This quote already exists!');
    }

    const quote: Quote = await this.quoteRepository.findOne({id : quoteId});

    if (!quote) {
      throw new BadRequestException('You are trying to modify a quote with non-existing id!');
    }

    quote.quotetext = quoteData.quotetext;

    await this.quoteRepository.save(quote);

    return quote;
  }
}
