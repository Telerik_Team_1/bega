import { Test, TestingModule } from '@nestjs/testing';
import { MoviesController } from './movies.controller';
import { MoviesService } from './movies.service';
import { MovieAddSchema } from '../models/movie/movie-add.schema';
import { User } from '../data/entities/user.entity';
import { PassportModule } from '@nestjs/passport';

jest.mock('./movies.service');

describe('MoviesController', () => {
  let module: TestingModule;
  let controller: MoviesController;
  const moviesService: MoviesService = new MoviesService(null);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [PassportModule.register({ defaultStrategy: 'jwt' })],
      controllers: [MoviesController],
      providers: [
        {
          provide: 'MoviesService',
          useValue: moviesService,
        },
      ],
    }).compile();
    controller = module.get<MoviesController>(MoviesController);
  });
  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('.getAllMovies ', () => {

    it('should call MoviesService getAll method', () => {

      // Arrange
      jest.spyOn(moviesService, 'getAll');

      // Act
      controller.getAllMovies();

     // Assert
      expect(moviesService.getAll).toHaveBeenCalledTimes(1);
    });

    it('should return what it received from moviesService.', async () => {

      // Arrange
      jest.spyOn(moviesService, 'getAll').mockImplementation(() => {
        return 'Movie';
      });

      // Act
      const result = await controller.getAllMovies();

     // Assert
      expect(result).toBe('Movie');
    });
  });

  describe('.addMovie', () => {
    const newMovie: MovieAddSchema = {title: 'Title'};
    const user = {user: new User()};

    it(' should call MoviesService.add method', async () => {

      // Arrange
      jest.spyOn(moviesService, 'add');

      // Act
      await controller.addMovie(user, newMovie);

      // Assert
      expect(moviesService.add).toHaveBeenCalledTimes(1);
    });

    it(' should return what it received from MoviesService.add method', async () => {

      // Arrange
      jest.spyOn(moviesService, 'add').mockImplementation((movieSent) => {
        return movieSent;
      });

      // Act
      const result = await controller.addMovie(user, newMovie);

      // Assert
      expect(result).toBe(newMovie);
    });
  });

  describe('.getById ', () => {

    it('should call MoviesService getById method.', () => {

      // Arrange
      jest.spyOn(moviesService, 'getById');

      // Act
      controller.getById(1);

      // Assert
      expect(moviesService.getById).toHaveBeenCalledTimes(1);
    });

    it('should return what it received from MoviesService getById method.', async () => {

      // Arrange
      jest.spyOn(moviesService, 'getById').mockReturnValue('Movie');

      // Act
      const result = await controller.getById(1);

      // Assert
      expect(result).toBe('Movie');
    });
  });

  describe('.delteById', () => {

    it(' should call MoviesService deleteById method', () => {

      // Arrange
      jest.spyOn(moviesService, 'deleteById');

      // Act
      controller.deleteById(1);

      // Assert
      expect(moviesService.deleteById).toHaveBeenCalledTimes(1);
    });

    it('should return what it received from MoviesService deleteById method', async () => {

      // Arrange
      const movie = 'Movie';
      jest.spyOn(moviesService, 'deleteById').mockReturnValue(movie);

      // Act
      const result = await controller.deleteById(1);

      // Assert
      expect(result).toBe(movie);
    });
  });

  describe('.updateById ', () => {

    const movieData: MovieAddSchema = {title: 'Title'};

    it('should call MoviesService updateById method', () => {

      // Arrange
      jest.spyOn(moviesService, 'updateById');

      // Act
      controller.updateById(1, movieData);

      // Assert
      expect(moviesService.updateById).toHaveBeenCalledTimes(1);
    });

    it('should return what it received from MoviesService updateById method', async () => {

      // Arrange
      jest.spyOn(moviesService, 'updateById').mockReturnValue('Movie');

      // Act
      const result = await controller.updateById(1, movieData);

      // Assert
      expect(result).toBe('Movie');
    });

  });
});