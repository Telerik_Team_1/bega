import { Request, Controller, Param, UseGuards, Delete, Get, Body, ParseIntPipe, Put, ValidationPipe, Patch } from '@nestjs/common';
import { MoviesService } from './movies.service';
import { AuthGuard } from '@nestjs/passport';
import { MovieSchema } from '../models/movie/movie.schema';
import { MovieAddSchema } from '../models/movie/movie-add.schema';
import { Roles, RolesGuard, EditMovie } from '../common';
@Controller('movies')
@UseGuards(AuthGuard(), RolesGuard)
export class MoviesController {

  constructor(
    private readonly moviesService: MoviesService,
  ) {}

  @Get()
  @Roles('admin', 'user')
  async getAllMovies(): Promise<MovieSchema[]> {

    return await this.moviesService.getAll();
  }

  @Put()
  @Roles('admin')
  async addMovie(
    @Request() req: any,
    @Body(new ValidationPipe({
      transform: true,
      whitelist: true,
    })) newMovie: MovieAddSchema,
  ): Promise<MovieSchema> {

    try {
      return await this.moviesService.add(newMovie, req.user);
    } catch (error) {
      return (error.message);
    }
  }

    @Get('/:id')
    @Roles('admin', 'user')
    async getById(@Param('id', new ParseIntPipe()) id: number): Promise<MovieSchema> {

    return await this.moviesService.getById(id);
  }

    @Delete('/:id')
    @Roles('admin')
    async deleteById(@Param('id', new ParseIntPipe()) id: number): Promise<MovieSchema> {

    return await this.moviesService.deleteById(id);
  }

  @Patch('/:id')
  @Roles('admin', 'user')
  @UseGuards(EditMovie)
  async updateById(
    @Param('id', new ParseIntPipe()) movieId: number,
    @Body(new ValidationPipe({
      transform: true,
      whitelist: true,
    })) movie: MovieAddSchema,
  ): Promise<MovieSchema> {

    return await this.moviesService.updateById(movieId, movie);
  }

}
