import { Test, TestingModule } from '@nestjs/testing';
import { MoviesService } from './movies.service';
import { Movie } from '../data/entities/movie.entity';
import { MovieAddSchema } from '../models/movie/movie-add.schema';
import { User } from '../data/entities/user.entity';

describe('MoviesService', () => {
  let service: MoviesService;
  const movieRepository = {
    find: () => {
      return 'movie';
    },
    findOne: () => {
      return 'movie';
    },
    create: () => {
      return 'movie';
    },
    save: () => {
      return 'movie';
    },
    remove: () => {
      return 'movie';
    },
  };

  beforeAll(async () => {

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MoviesService,
        {
          provide: 'MovieRepository',
          useValue: movieRepository,
        },
      ],
    }).compile();

    service = module.get<MoviesService>(MoviesService);
  });

  afterEach(async () => {
    jest.clearAllMocks();
  });

  it(' should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('.getAll ', () => {

    it(' should call MoviesRepository.find ', async () => {

      // Arrange
      jest.spyOn(movieRepository, 'find');

      // Act
      await service.getAll();

      // Assert
      expect(movieRepository.find).toHaveBeenCalledTimes(1);
    });

    it(' should return correct data', async () => {

      // Arrange
      const movie = new Movie();
      jest.spyOn(movieRepository, 'find').mockReturnValue(movie);

      // Act
      const result = await service.getAll();

      // Assert
      expect(result).toBe(movie);
    });
  });

  describe('getById(parameter)', () => {

    it('should call repository.findOne', () => {

      // Arrange
      jest.spyOn(movieRepository, 'findOne');

      // Act
      service.getById(1);

      // Assert
      expect(movieRepository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should return correct data', async () => {

      // Arrange
      const movie = new Movie();
      jest.spyOn(movieRepository, 'findOne').mockReturnValue(movie);

      // Act
      const result = await service.getById(1);

      // Assert
      expect(result).toBe(movie);
    });

    it('should trow if no movie is found', async () => {

      // Arrange
      let result;
      jest.spyOn(movieRepository, 'findOne').mockReturnValue(undefined);

      // Act
      try{
        await service.getById(1);
      } catch (error) {
        result = error.message.message;
      }

      // Assert
      expect(result).toBe('The movie you are trying to access does not exist!');
    });
  });

  describe('.add ', () => {

    const movieData: MovieAddSchema = {title: 'Title'};
    const user: User = new User();

    it('to make correct calls to repository', async () => {

      // Arrange
      jest.spyOn(movieRepository, 'findOne');
      jest.spyOn(movieRepository, 'create');
      jest.spyOn(movieRepository, 'save');

      const movieToAdd = new Movie();
      movieToAdd.user = user;
      movieToAdd.title = movieData.title;

      // Act
      await service.add(movieData, user);

      // Assert
      expect(movieRepository.findOne).toHaveBeenCalledTimes(2);
      expect(movieRepository.findOne).toHaveBeenCalledWith({ where: { title: movieData.title } });
      expect(movieRepository.create).toHaveBeenCalledTimes(1);
      expect(movieRepository.create).toHaveBeenCalledWith(movieToAdd);
      expect(movieRepository.save).toHaveBeenCalledTimes(1);
      expect(movieRepository.save).toHaveBeenCalledWith(movieToAdd);
    });

    it(' should trow when movie already exist.', async () => {

      // Arrange
      jest.spyOn(movieRepository, 'findOne').mockReturnValue(true);
      let result = null;

      // Act
      try{
        await service.add(movieData, user);
      } catch (error) {
        result = error.message.message;
      }

      // Assert
      expect(result).toBe('This movie already exists!');
    });
  });

  describe('.deleteById ', () => {

    it(' should call MoviesRepository.findOne with parameter provided ', async () => {

      // Arrange
      jest.spyOn(movieRepository, 'findOne');

      // Act
      await service.deleteById(2);

      // Assert
      expect(movieRepository.findOne).toHaveBeenCalledTimes(1);
      expect(movieRepository.findOne).toHaveBeenCalledWith({id: 2});
    });

    it(' should call MoviesRepository.remove with parameter provided', async () => {

      // Arrange
      const movie = new Movie();
      jest.spyOn(movieRepository, 'findOne').mockReturnValue(movie);
      jest.spyOn(movieRepository, 'remove');

      // Act
      await service.deleteById(1);

      // Assert
      expect(movieRepository.remove).toBeCalledTimes(1);
      expect(movieRepository.remove).toBeCalledWith(movie);
    });

    it(' should throw if movie does not exist.', async () => {

      // Arrange
      let result = null;
      jest.spyOn(movieRepository, 'findOne').mockReturnValue(undefined);
      jest.spyOn(movieRepository, 'remove');

      // Act
      try {
        await service.deleteById(1);
      } catch (error) {
        result = error.message.message;
      }

      // Assert
      expect(result).toBe(`The movie you are trying to delete does not exist!`);
    });
  });

  describe('.updateById ', () => {

    const movieData: MovieAddSchema = {title: 'Title'};

    it(' should trow when movie exists ', async () => {

      // Arrange
      jest.spyOn(movieRepository, 'findOne').mockReturnValue(true);
      let result = null;

      // Act
      try {
        await service.updateById(2, movieData);
      } catch (error) {
        result = error.message.message;
      }

      // Assert
      expect(result).toBe('A movie title already exists!');
    });

    it(' should trow when movie does not exist ', async () => {

      // Arrange
      jest.spyOn(movieRepository, 'findOne').mockReturnValue(false);
      let result = null;

      // Act
      try {
        await service.updateById(2, movieData);
      } catch (error) {
        result = error.message.message;
      }

      // Assert
      expect(result).toBe('You are trying to modify a movie with non-existing id!');
    });
  });
});
