import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Movie } from '../data/entities/movie.entity';
import { Repository, getRepository } from 'typeorm';
import { MovieAddSchema } from '../models/movie/movie-add.schema';
import { User } from '../data/entities/user.entity';
@Injectable()
export class MoviesService {
  constructor(
    @InjectRepository(Movie)
    private readonly movieRepository: Repository<Movie>,
  ) {}

  async getAll(): Promise<Movie[]> {

    return await this.movieRepository.find({});
  }

  async getById(id: number): Promise<Movie> {

    const movieFound: Movie = await this.movieRepository.findOne({ where: { id } });

    if (movieFound) {
      return movieFound;
    } else {
      throw new BadRequestException(`The movie you are trying to access does not exist!`);
    }
  }
  async add(movieData: MovieAddSchema, user: User): Promise<Movie>{

    const movieExists = await this.movieRepository.findOne({ where: { title: movieData.title } });

    if (movieExists) {
      throw new BadRequestException('This movie already exists!');
    }

    const movieToAdd = new Movie();
    movieToAdd.title = movieData.title;
    movieToAdd.user = user;

    await this.movieRepository.create(movieToAdd);
    await this.movieRepository.save(movieToAdd);

    return await this.movieRepository.findOne({ where: { title: movieData.title } });
  }

  async deleteById(id: number): Promise<Movie> {

    const movieToDelete: Movie = await this.movieRepository.findOne({ id });

    if (movieToDelete) {
      await this.movieRepository.remove(movieToDelete);
      return movieToDelete;
    } else {
      throw new BadRequestException(`The movie you are trying to delete does not exist!`);
    }
  }

  async updateById(movieId: number, movieData: MovieAddSchema): Promise<Movie> {

    const titleExists = await this.movieRepository.findOne({title: movieData.title});
    if (titleExists) {
      throw new BadRequestException('A movie title already exists!');
    }

    const movie: Movie = await this.movieRepository.findOne({id : movieId});
    if (!movie) {
      throw new BadRequestException('You are trying to modify a movie with non-existing id!');
    }

    movie.title = movieData.title;

    await this.movieRepository.save(movie);

    return movie;
  }
}
