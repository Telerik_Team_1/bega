import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Quote } from 'src/data/entities/quote.entity';

@Injectable()
export class EditQhote implements CanActivate {

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();

    const user = request.user;
    const id = +request.params.id;
    const isOwner = () => user.quotes.find((quote: Quote) => quote.id === id);

    return user && id && isOwner();
  }
}