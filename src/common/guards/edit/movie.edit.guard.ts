import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Movie } from 'src/data/entities/movie.entity';

@Injectable()
export class EditMovie implements CanActivate {

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();

    const user = request.user;
    const id = +request.params.id;
    const isOwner = () => user.movies.find((movie: Movie) => movie.id === id);

    return user && id && isOwner();
  }
}