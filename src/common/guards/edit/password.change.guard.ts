import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import {  } from 'src/data/entities/movie.entity';

@Injectable()
export class ChangePass implements CanActivate {

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();

    const user = request.user;
    const id = +request.params.id;

    return user && id && user.id === id;
  }
}