import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { Repository, TransactionManager, EntityManager, Transaction, getRepository } from 'typeorm';
import { User } from './../../data/entities/user.entity';
import { InjectRepository, InjectEntityManager } from '@nestjs/typeorm';

import * as bcrypt from 'bcrypt';
import { IJwtPayload } from './../../interfaces/jwt-payload';
import { validate } from 'class-validator';
import { UserRegisterSchema } from '../../models/user/user-register.schema';
import { UserSchema } from '../../models/user/user.schema';
import { UserLoginSchema } from '../../models/user/user-login.schema';
import { Role } from '../../data/entities/roles.entity';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) { }

  async registerUser(user: UserRegisterSchema) {

    const userFound = await this.usersRepository.findOne({ where: { email: user.email } });

    if (userFound) {
      throw new BadRequestException('There is already such user registered!');
    }

    user.dateRegistered = new Date();
    user.lastActive = new Date();
    user.password = await bcrypt.hash(user.password, 10);
    user.role = await getRepository(Role).findOne({roleName: 'user'});

    await this.usersRepository.create(user);

    const result = await this.usersRepository.save(user);

    return result;
  }

  async validateUser(payload: IJwtPayload): Promise<UserSchema> {

    const userFound: UserSchema = await this.usersRepository.findOne({ where: { email: payload.email } });

    return userFound;
  }

  async signIn(user: UserLoginSchema): Promise<UserSchema> {

    const userFound: UserSchema = await this.usersRepository.findOne({ /* select: ['email', 'role', 'password'],*/ where: { email: user.email } });

    if (userFound) {
      const result = await bcrypt.compare(user.password, userFound.password);
      if (result) {
        return userFound;
      }
    }

    throw new NotFoundException('Wrong credentials');
  }

  async getAll() {
    return this.usersRepository.find({});
  }

  async getById(ID: number): Promise<UserSchema> {

    const userFound: UserSchema = await this.usersRepository.findOne({ where: { id: ID } });

    if (userFound) {
      return userFound;
    } else {
      throw new BadRequestException(`The user you're trying to find does not exist!`);
    }
  }

  async updateRole(id: number, role: string): Promise<UserSchema>{

    const userFound: UserSchema = await this.getById(id);

    userFound.role = await getRepository(Role).findOne({roleName: role});

    this.usersRepository.save(userFound);

    return userFound;
  }

  async updatePassword(id: number, passwords: {old: string, new: string}): Promise<UserSchema> {

    const userFound = await this.getById(id);

    if (await bcrypt.compare(passwords.old, userFound.password)) {
      userFound.password = await bcrypt.hash(passwords.new, 10);
      this.usersRepository.save(userFound);
      return userFound;
    }

    throw new NotFoundException('Wrong credentials');
  }
}
