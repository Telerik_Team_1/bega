import { UserRegisterSchema } from './../models/user/user-register.schema';
import { UserLoginSchema } from '../models/user/user-login.schema';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { PassportModule } from '@nestjs/passport';
import { UsersService } from '../common/core/users.service';

jest.mock('./auth.service');
jest.mock('../common/core/users.service.ts');

describe('AuthController', () => {
  let module: TestingModule;
  let controller: AuthController;
  const authService: AuthService = new AuthService(null, null);
  const usersService: UsersService = new UsersService(null);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [PassportModule.register({ defaultStrategy: 'jwt' })],
      controllers: [AuthController],
      providers: [
        {
          provide: 'AuthService',
          useValue: authService,
        },
        {
          provide: 'UsersService',
          useValue: usersService,
        },
      ],
    }).compile();
    controller = module.get<AuthController>(AuthController);
  });

  describe('zerro test', () => {

    it('should pass (controller is defined)', () => {
      expect(controller).toBeDefined();
    });

  });

  describe('get:root ', () => {

    it(`root should return 'root'`, () => {

      // Arrange & Act
      const result = controller.root();

      // Assert
      expect(result).toBe('root');
    });

  });

  describe('get:login ', () => {

    it(`should call authService.signIn`, async () => {

      // Arrange
      const user = new UserLoginSchema();
      jest.spyOn(authService, 'signIn').mockImplementation(async () => {
        return 'token';
      });

      // Act
      const result = await controller.sign(user);

      // Assert
      expect(authService.signIn).toHaveBeenCalledTimes(1);
    });

    it(`should return 'token'`, async () => {

      // Arrange
      const user = new UserLoginSchema();
      jest.spyOn(authService, 'signIn').mockImplementation(async () => {
        return 'token';
      });

      // Act
      const result = await controller.sign(user);

      // Assert
      expect(result).toBe('token');
    });

  });

  describe('post:register', () => {

    it(`should call registerUser`, async () => {

      // Arrange
      const user = new UserRegisterSchema();
      jest.spyOn(usersService, 'registerUser').mockImplementation(async () => true);

      // Act
      const result = await controller.register(user);

      // Assert
      expect(usersService.registerUser).toHaveBeenCalledTimes(1);

    });

    it(`should return 'saved' when the user is registered successfully`, async () => {

      // Arrange
      const user = new UserRegisterSchema();
      jest.spyOn(usersService, 'registerUser').mockImplementation(async () => true);

      // Act
      const result = await controller.register(user);

      // Assert
      expect(result).toEqual({ message: 'saved' });

    });

    it(`should return the error message`, async () => {

      // Arrange
      const user = new UserRegisterSchema();
      jest.spyOn(usersService, 'registerUser').mockImplementation(async () => { throw new Error('test'); });

      // Act
      const result = await controller.register(user);

      // Assert
      expect(result).toBeInstanceOf(Error);

    });

  });

});