import { AdminGuard } from './../common';
import { UsersService } from '../common/core/users.service';
import { AuthService } from './auth.service';
import { Get, Controller, UseGuards, Post, Body, FileInterceptor, UseInterceptors, UploadedFile, ValidationPipe, UsePipes } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { join } from 'path';
import { unlink } from 'fs';
import { UserLoginSchema } from '../models/user/user-login.schema';
import { UserRegisterSchema } from '../models/user/user-register.schema';

@Controller('auth')
export class AuthController {

  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) { }

  @Get()
  @UseGuards(AuthGuard(), AdminGuard)
  root(): string {
    return 'root';
  }

  @Get('login')
  async sign(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) user: UserLoginSchema): Promise<string> {
    return await this.authService.signIn(user);
  }

  @Post('register')
  async register(
    @Body(new ValidationPipe({
      transform: true,
      whitelist: true,
    }))
    user: UserRegisterSchema,
  ): Promise<{ message: string }> {

    try {
      await this.usersService.registerUser(user);
      return { message: 'saved' };
    } catch (error) {
      return error;
    }
  }
}
