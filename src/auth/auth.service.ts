import { UsersService } from '../common/core/users.service';
import { JwtService } from '@nestjs/jwt';
import { Injectable, NotFoundException } from '@nestjs/common';
import { IJwtPayload } from './../interfaces/jwt-payload';
import { UserLoginSchema } from '../models/user/user-login.schema';
import { UserSchema } from '../models/user/user.schema';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) { }

  public async signIn(user: UserLoginSchema): Promise<string> {
    const userFound = await this.usersService.signIn(user);
    if (userFound) {
      return this.jwtService.sign({ email: userFound.email, role: userFound.role.roleName });
    } else {
      throw new NotFoundException('Wrong credentials');
    }
  }

  async validateUser(payload: IJwtPayload): Promise<UserSchema> {
    return await this.usersService.validateUser(payload);
  }
}
