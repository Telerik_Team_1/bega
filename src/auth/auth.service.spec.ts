import { UserSchema } from '../models/user/user.schema';
import { UserLoginSchema } from './../models/user/user-login.schema';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from './../common/core/users.service';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { IJwtPayload } from '../interfaces/jwt-payload';

describe('AuthService', () => {
  let service: AuthService;
  const usersService = new UsersService(null);
  const jwtService = new JwtService(null);

  beforeAll(async () => {

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: 'UsersService',
          useValue: usersService,
        },
        {
          provide: 'JwtService',
          useValue: jwtService,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  afterEach(async () => {
    jest.clearAllMocks();
  });

  describe(`zero test`, () => {

    it(' should be defined', () => {
      expect(service).toBeDefined();
    });

  });

  describe(`signIn`, () => {

    it(`should call usersService.signIn`, async () => {

      // Arrange
      const user = new UserLoginSchema();
      jest.spyOn(jwtService, 'sign').mockImplementation(async () => 'token');
      jest.spyOn(usersService, 'signIn').mockImplementation(async () => {
        return {
          email: 'email',
          role: { roleName: 'user' },
        };
      });

      // Act
      await service.signIn(user);

      // Assert
      expect(usersService.signIn).toHaveBeenCalledTimes(1);

    });

    it(`should return 'token' when the user is signed in`, async () => {

      // Arrange
      const user = new UserLoginSchema();
      jest.spyOn(jwtService, 'sign').mockImplementation(async () => 'token');
      jest.spyOn(usersService, 'signIn').mockImplementation(async () => {
        return {
          email: 'email',
          role: { roleName: 'user' },
        };
      });

      // Act
      const result = await service.signIn(user);

      // Assert
      expect(result).toBe('token');

    });

    it(`should throw if no valid user data is passed`, async () => {

      // Arrange
      let message = '';
      const user = new UserLoginSchema();
      jest.spyOn(usersService, 'signIn').mockImplementation(async () => false);

      // Act
      try {
        const result = await service.signIn(user);
      } catch (error) {
        message = error.message.message;
      }

      // Assert
      expect(message).toBe('Wrong credentials');

    });

  });

  describe(`validateUser`, () => {

    it(`should call usersService.validateUser`, async () => {

      // Arrange
      const user = new UserSchema();
      const payload: IJwtPayload = { email: 'test' };
      jest.spyOn(usersService, 'validateUser').mockImplementation(async () => user);

      // Act
      const result = await service.validateUser(payload);

      // Assert
      expect(usersService.validateUser).toHaveBeenCalledTimes(1);

    });

    it(`should return correct data when the user is validate`, async () => {

      // Arrange
      const user = new UserSchema();
      const payload: IJwtPayload = { email: 'test' };
      jest.spyOn(usersService, 'validateUser').mockImplementation(async () => user);

      // Act
      const result = await service.validateUser(payload);

      // Assert
      expect(result).toEqual(user);

    });

  });

});
