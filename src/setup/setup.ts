import 'reflect-metadata';
import { createConnection} from 'typeorm';
import { Role } from '../data/entities/roles.entity';
import { ApiRoute } from '../data/entities/apiroute.entity';

createConnection().then(async (conn) => {
  // Setup user roles
  const roleRepo = conn.getRepository<Role>(Role);

  try {
    const roleUser = new Role();
    roleUser.roleName = 'user';
    await roleRepo.save(roleUser);

    const roleAdmin = new Role();
    roleAdmin.roleName = 'admin';
    await roleRepo.save(roleAdmin);

    const roleBanned = new Role();
    roleBanned.roleName = 'banned';
    await roleRepo.save(roleBanned);

    console.log(`User roles set up successfully.`);
  } catch (e) {
    console.warn(e);
  }

  // Setup api routes table
  const routeRepo = conn.getRepository<ApiRoute>(ApiRoute);

  try {
    const randomRoute = new ApiRoute();
    randomRoute.route = '/api/random';
    await routeRepo.save(randomRoute);

    const allQuotesRoute = new ApiRoute();
    allQuotesRoute.route = '/api/all/:id';
    await routeRepo.save(allQuotesRoute);

    const quoteByIdRoute = new ApiRoute();
    quoteByIdRoute.route = '/api/:id';
    await routeRepo.save(quoteByIdRoute);

    const randomByMovie = new ApiRoute();
    randomByMovie.route = '/api/random/:id';
    await routeRepo.save(randomByMovie);

    console.log(`Routes table set up successfully.`);
  } catch (e) {
    console.warn(e.message);
  }

  conn.close();
});
