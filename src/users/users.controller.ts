import { AuthGuard } from '@nestjs/passport';
import { Controller, Get, UseGuards, Patch, Body, BadRequestException, Param, ParseIntPipe } from '@nestjs/common';
import { UsersService } from './../common/core/users.service';
import { Roles, RolesGuard, ChangePass } from '../common';
import { UserSchema } from '../models/user/user.schema';

@Controller('users')
@UseGuards(AuthGuard(), RolesGuard)
export class UsersController {

  constructor(
    private readonly usersService: UsersService,
  ) { }

  @Get()
  @Roles('admin')
  async all() {

    return await this.usersService.getAll();
  }

  @Get(':id')
  @Roles('admim', 'user')
  async getById(@Param('id', new ParseIntPipe()) id: number) {

    return await this.usersService.getById(id);
  }

  @Patch('/:id/role')
  @Roles('admin')
  async updateRole(
      @Param('id', new ParseIntPipe()) id: number,
      @Body() role: {roleName: string},
    ): Promise<UserSchema> {

    if (['admin', 'user', 'banned'].includes(role.roleName) === false) {
      throw new BadRequestException('New role needs to be "admin", "user" or "banned"');
    }

    return await this.usersService.updateRole(id, role.roleName);
  }

  @Patch('/:id/password')
  @Roles('user', 'admin')
  @UseGuards(ChangePass)
  async updatePassword(
      @Param('id', new ParseIntPipe()) id: number,
      @Body() passwords: {old: string, new: string},
    ): Promise<UserSchema> {

    return await this.usersService.updatePassword(id, passwords);
  }
}
