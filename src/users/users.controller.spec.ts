import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from '../common/core/users.service';
import { PassportModule } from '@nestjs/passport';

jest.mock('../common/core/users.service');
describe('Users Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    const usersService = new UsersService(null);
    module = await Test.createTestingModule({
      imports: [PassportModule.register({ defaultStrategy: 'jwt' })],
      controllers: [UsersController],
      providers: [
        {
          provide: 'UsersService',
          useValue: usersService,
        },
      ],
    }).compile();
  });
  it('should be defined', () => {
    const controller: UsersController = module.get<UsersController>(UsersController);
    expect(controller).toBeDefined();
  });
});
