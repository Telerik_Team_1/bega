import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, JoinTable } from 'typeorm';
import { User } from './user.entity';
import { Movie } from './movie.entity';

@Entity({
  name: 'quotes',
})
export class Quote {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'nvarchar', length: 255})
  quotetext: string;

  @Column()
  status: number;

  @ManyToOne(type => User, user => user.quotes)
  user: Promise<User>;

  @ManyToOne(type => Movie, movie => movie.quotes, {onDelete: 'CASCADE'})
  @JoinTable()
  movie: Promise<Movie>;
}