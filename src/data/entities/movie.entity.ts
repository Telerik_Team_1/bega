import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany } from 'typeorm';
import { IsEmail } from 'class-validator';
import { User } from './user.entity';
import { Quote } from './quote.entity';

@Entity({
  name: 'movies',
})
export class Movie {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'nvarchar', length: 255})
  title: string;

  @OneToMany(type => Quote, quote => quote.movie, {
    cascade: true,
    eager: true,
  })
  quotes: Quote[];

  @ManyToOne(type => User, user => user.quotes, {
    // cascade: true,
    // eager: true,
    // nullable: false,  : it should be not nullable, but I can't drop foreign key and there should be a better way than reinitializing migrations
  })
  user: User;
}
