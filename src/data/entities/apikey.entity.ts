import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany, ManyToMany } from 'typeorm';
import { User } from './user.entity';
import { ApiRoute } from './apiroute.entity';
import { KeyUsage } from './keyusage.entity';

@Entity({
  name: 'apikeys',
})
export class ApiKey {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'nvarchar', length: 255})
  apikey: string;

  @ManyToOne(type => User, user => user.apikeys, {
    eager: true,
  })
  user: User;

  @OneToMany(type => KeyUsage, usage => usage.apikey, {
    eager: true,
  })
  usage: KeyUsage[];

}
