import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany, ManyToMany } from 'typeorm';
import { User } from './user.entity';
import { ApiRoute } from './apiroute.entity';
import { KeyUsage } from './keyusage.entity';

@Entity({
  name: 'stages',
})
export class Stage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({unique: true})
  level: number;

  @Column()
  completeDate: Date;

}
