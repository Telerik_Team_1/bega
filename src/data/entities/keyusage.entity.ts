import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany, ManyToMany } from 'typeorm';
import { User } from './user.entity';
import { ApiRoute } from './apiroute.entity';
import { ApiKey } from './apikey.entity';

@Entity({
  name: 'keyusage',
})
export class KeyUsage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  usedOn: Date;

  @ManyToOne(type => ApiKey, apikey => apikey.usage)
  apikey: ApiKey;

  @ManyToMany(type => ApiRoute, route => route.usage, {
    eager: true,
  })
  routes: ApiRoute[];

}
