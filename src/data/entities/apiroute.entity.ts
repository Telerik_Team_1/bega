import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany, ManyToMany } from 'typeorm';
import { User } from './user.entity';
import { ApiKey } from './apikey.entity';
import { KeyUsage } from './keyusage.entity';

@Entity({
  name: 'apiroutes',
})
export class ApiRoute {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'nvarchar', length: 255, unique: true})
  route: string;

  @ManyToMany(type => KeyUsage, usage => usage.routes)
  usage: KeyUsage[];

}
