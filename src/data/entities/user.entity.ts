import { Column, PrimaryGeneratedColumn, Entity, OneToMany, ManyToMany, ManyToOne } from 'typeorm';
import { IsEmail } from 'class-validator';
import { Quote } from './quote.entity';
import { Movie } from './movie.entity';
import { Role } from './roles.entity';
import { ApiKey } from './apikey.entity';

@Entity({
  name: 'users',
})
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'nvarchar', length: 255})
  username: string;

  @Column()
  @IsEmail()
  email: string;

  @Column()
  password: string;

  @Column()
  dateRegistered: Date;

  @Column()
  lastActive: Date;

  @OneToMany(type => Quote, quote => quote.user, {eager: true})
  quotes: Quote[];

  @OneToMany(type => Movie, movie => movie.user, {eager: true})
  movies: Movie[];

  @OneToMany(type => ApiKey, apikey => apikey.user)
  apikeys: ApiKey[];

  @ManyToOne(type => Role, role => role.users, {
    nullable: false,
    eager: true,
  })
  role: Role;
}
