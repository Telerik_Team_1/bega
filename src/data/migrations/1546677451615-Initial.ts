import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1546677451615 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `movies` (`id` int NOT NULL AUTO_INCREMENT, `title` varchar(255) NOT NULL, `userId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `quotes` (`id` int NOT NULL AUTO_INCREMENT, `quotetext` varchar(255) NOT NULL, `status` int NOT NULL, `userId` int NULL, `movieId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `roles` (`id` int NOT NULL AUTO_INCREMENT, `roleName` varchar(255) NOT NULL, UNIQUE INDEX `IDX_992f24b9d80eb1312440ca577f` (`roleName`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`id` int NOT NULL AUTO_INCREMENT, `username` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `dateRegistered` datetime NOT NULL, `lastActive` datetime NOT NULL, `roleId` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `apiroutes` (`id` int NOT NULL AUTO_INCREMENT, `route` varchar(255) NOT NULL, UNIQUE INDEX `IDX_74893b645b9f4ff0a2e07f6141` (`route`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `keyusage` (`id` int NOT NULL AUTO_INCREMENT, `usedOn` datetime NOT NULL, `apikeyId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `apikeys` (`id` int NOT NULL AUTO_INCREMENT, `apikey` varchar(255) NOT NULL, `userId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `stages` (`id` int NOT NULL AUTO_INCREMENT, `level` int NOT NULL, `completeDate` datetime NOT NULL, UNIQUE INDEX `IDX_3b5128c6d279b4e36ad83a6cee` (`level`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `movies` ADD CONSTRAINT `FK_64a78407424745d6c053e93cc36` FOREIGN KEY (`userId`) REFERENCES `users`(`id`)");
        await queryRunner.query("ALTER TABLE `quotes` ADD CONSTRAINT `FK_8bad8bd49d1dd6954b46366349c` FOREIGN KEY (`userId`) REFERENCES `users`(`id`)");
        await queryRunner.query("ALTER TABLE `quotes` ADD CONSTRAINT `FK_7ea831fe165af8a507e32fe8b10` FOREIGN KEY (`movieId`) REFERENCES `movies`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_368e146b785b574f42ae9e53d5e` FOREIGN KEY (`roleId`) REFERENCES `roles`(`id`)");
        await queryRunner.query("ALTER TABLE `keyusage` ADD CONSTRAINT `FK_7b836f7844b54258d4b74f62221` FOREIGN KEY (`apikeyId`) REFERENCES `apikeys`(`id`)");
        await queryRunner.query("ALTER TABLE `apikeys` ADD CONSTRAINT `FK_143253b7d426c598cc9913783ef` FOREIGN KEY (`userId`) REFERENCES `users`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `apikeys` DROP FOREIGN KEY `FK_143253b7d426c598cc9913783ef`");
        await queryRunner.query("ALTER TABLE `keyusage` DROP FOREIGN KEY `FK_7b836f7844b54258d4b74f62221`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_368e146b785b574f42ae9e53d5e`");
        await queryRunner.query("ALTER TABLE `quotes` DROP FOREIGN KEY `FK_7ea831fe165af8a507e32fe8b10`");
        await queryRunner.query("ALTER TABLE `quotes` DROP FOREIGN KEY `FK_8bad8bd49d1dd6954b46366349c`");
        await queryRunner.query("ALTER TABLE `movies` DROP FOREIGN KEY `FK_64a78407424745d6c053e93cc36`");
        await queryRunner.query("DROP INDEX `IDX_3b5128c6d279b4e36ad83a6cee` ON `stages`");
        await queryRunner.query("DROP TABLE `stages`");
        await queryRunner.query("DROP TABLE `apikeys`");
        await queryRunner.query("DROP TABLE `keyusage`");
        await queryRunner.query("DROP INDEX `IDX_74893b645b9f4ff0a2e07f6141` ON `apiroutes`");
        await queryRunner.query("DROP TABLE `apiroutes`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP INDEX `IDX_992f24b9d80eb1312440ca577f` ON `roles`");
        await queryRunner.query("DROP TABLE `roles`");
        await queryRunner.query("DROP TABLE `quotes`");
        await queryRunner.query("DROP TABLE `movies`");
    }

}
