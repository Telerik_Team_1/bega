import { Role } from 'src/data/entities/roles.entity';

export class UserSchema {

  username: string;

  email: string;

  password: string;

  role: Role;

  dateRegistered: Date;

  lastActive: Date;

}