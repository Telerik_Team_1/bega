import { IsEmail, IsString, Matches, IsAlphanumeric, Length, IsOptional } from 'class-validator';
import { Role } from '../../data/entities/roles.entity';

export class UserRegisterSchema {

  @IsAlphanumeric()
  @Length(6, 40)
  username: string;

  @IsEmail()
  email: string;

  @IsString()
  @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  password: string;

  @IsOptional()
  dateRegistered: Date;

  @IsOptional()
  lastActive: Date;

  @IsOptional()
  role: Role;

}