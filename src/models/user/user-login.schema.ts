import { IsEmail, IsString, Matches, IsAlphanumeric } from 'class-validator';

export class UserLoginSchema {

  @IsEmail()
  email: string;

  @IsString()
  @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  password: string;

}