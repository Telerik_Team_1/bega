import { IsString, Length } from 'class-validator';

export class QuoteAddSchema {
  @IsString()
  @Length(5, 500)
  quotetext: string;
}