import { Min, Length } from 'class-validator';
import { User } from 'src/data/entities/user.entity';
import { Movie } from 'src/data/entities/movie.entity';

export class QuoteSchema {
  @Min(1)
  id: number;

  @Length(5, 500)
  quotetext: string;

  status: number;
  user: User;
  movie: Movie;
}