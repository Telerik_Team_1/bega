
import { IsString, Length, IsInt, Min } from 'class-validator';

export class MovieAddSchema {
  @IsString()
  @Length(1, 200)
  title: string;
}