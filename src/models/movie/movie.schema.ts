
import { IsString, Length, IsInt, Min } from 'class-validator';
import { User } from 'src/data/entities/user.entity';
import { Optional } from '@nestjs/common';
import { Quote } from 'src/data/entities/quote.entity';

export class MovieSchema {
  id: number;
  title: string;
  quotes: Quote[];
  user: User;
}